/**
 * Given a non-negative integer x, compute and return the square root of x.

Since the return type is an integer, the decimal digits are truncated, and only the integer part of the result is returned.

Note: You are not allowed to use any built-in exponent function or operator, such as pow(x, 0.5) or x ** 0.5.

 

Example 1:

Input: x = 4
Output: 2
Example 2:

Input: x = 8
Output: 2
Explanation: The square root of 8 is 2.82842..., and since the decimal part is truncated, 2 is returned.
 */

/**
 * @param {number} x
 * @return {number}
 */
 var mySqrt = function(x) {

    let target = x;

    if(target === 0) return 0;
    if(target === 1) return 1;


    let start = 1;
    let end = x;
    while (start <= end) {
        let mid = parseInt(start + ((end-start)/2));
        if(mid*mid === target) return mid;
        if(mid*mid > target) end = mid - 1;
        if(mid*mid < target) start = mid + 1 ;
    }
    return end;
};


console.log(mySqrt(8));
console.log(mySqrt(4));

console.log(mySqrt(256));
