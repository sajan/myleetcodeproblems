/**
 * Given two binary strings a and b, return their sum as a binary string.

 

Example 1:

Input: a = "11", b = "1"
Output: "100"
Example 2:

Input: a = "1010", b = "1011"
Output: "10101"

 */

/**
 * @param {string} a
 * @param {string} b
 * @return {string}
 */
var addBinary = function (a, b) {
    // a.length
    // const arr1 = a.split('').map(x => Number(x));
    // const arr2 = b.split('').map(x => Number(x));

    let carry = 0;
    let sum = '';
    const l = Math.max(a.length, b.length);
    const al = l - a.length;
    const bl = l- b.length;
    for (let i = l-1; i >=0; i--) {

        let aval = isNaN(Number(a[i - al])) ? 0 : Number(a[i - al])
        let bval = isNaN(Number(b[i - bl])) ? 0 : Number(b[i - bl])
        let calc = aval + bval + carry;
        if(calc === 2) {
            calc = 0;
            carry = 1;
        } 
        else if (calc === 3) {
            calc = 1;
            carry = 1;
        }
        else {
            carry = 0;
        }
        sum = ''+calc + sum;

    }
    if(carry) {
        sum = '1' + sum;
    }
    return sum;
    
};


[
    // { a : "1010", b : "1011" },
    { a : "10101010111", b : "110011001111111" },
].forEach(x => {
    console.log(addBinary(x.a, x.b))
})