/**
 * 6. Zigzag Conversion
 * The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this: (you may want to display this pattern in a fixed font for better legibility)

P   A   H   N
A P L S I I G
Y   I   R
And then read line by line: "PAHNAPLSIIGYIR"

Write the code that will take a string and make this conversion given a number of rows:

string convert(string s, int numRows);
 

Example 1:

Input: s = "PAYPALISHIRING", numRows = 3
Output: "PAHNAPLSIIGYIR"
Example 2:

Input: s = "PAYPALISHIRING", numRows = 4
Output: "PINALSIGYAHRPI"
Explanation:
P     I    N
A   L S  I G
Y A   H R
P     I
Example 3:

Input: s = "A", numRows = 1
Output: "A"
 

Constraints:

1 <= s.length <= 1000
s consists of English letters (lower-case and upper-case), ',' and '.'.
1 <= numRows <= 1000
 */

/**
 * @param {string} s
 * @param {number} numRows
 * @return {string}
 */
 var convert = function(s, numRows) {
  
    if(numRows == 1) return s;
    
    let n = numRows;
    let map = {};
    for (let i = 1; i <= n; i++) {
        map[i] = []; 
    }

    let mode = true;  // true => straight & false => reverse
    let pointer = 1;

    for (let i = 0; i < s.length; i++) {
        const element = s[i];
        if(mode) {
            if(pointer < n){
                map[pointer].push(element)
                pointer++
            }
            else if(pointer === n) {
                map[pointer].push(element);
                mode = false;
                pointer--;
            }
        } else {
                if(pointer > 1) {
                map[pointer].push(element);
                pointer--;
            } 
            else if(pointer === 1) {
                map[pointer].push(element);
                pointer++;
                mode = true;
            }
        }
    }

    let str = "";
    for (const key in map) {
        if (Object.hasOwnProperty.call(map, key)) {
            const element = map[key];
            str +=map[key].join("")
        }
    }

    return str;
    
};


console.log(convert('PAYPALISHIRING', 6))