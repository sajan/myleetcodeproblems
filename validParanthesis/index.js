/**
 * Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

An input string is valid if:

Open brackets must be closed by the same type of brackets.
Open brackets must be closed in the correct order.
 

Example 1:

Input: s = "()"
Output: true
Example 2:

Input: s = "()[]{}"
Output: true
Example 3:

Input: s = "(]"
Output: false
Example 4:

Input: s = "([)]"
Output: false
Example 5:

Input: s = "{[]}"
Output: true
 

Constraints:

1 <= s.length <= 104
s consists of parentheses only '()[]{}'.
 */

var isValid = function(s) {
    const map = {
        '(': { type: 'round', state: 'open' },
        ')': { type: 'round', state: 'close' },
        '[': { type: 'square', state: 'open' },
        ']': { type: 'square', state: 'close' },
        '{': { type: 'curly', state: 'open' },
        '}': { type: 'curly', state: 'close' },
    }
    const stack = [];
    for (let i = 0; i < s.length; i++) {
        const element = s[i];

        if (stack.length ===  0) { 
            if(map[element].state === 'close') return false;
            stack.push(element) 
        }
        else {
            if(map[stack[stack.length-1]].type  === map[element].type && map[stack[stack.length-1]].state === 'open'  && map[element].state === 'close')  {
                stack.pop();
            } else {
                stack.push(element)
            }
        }
       
    }

    if(stack.length > 0) return false;
    return true
};



inputs  = [
    "(){}}{",
    "()",
    "()[]{}",
    "(]",
    "([)]",
    "{[]}",
    "{(){[]{()}}}",
    "][]["
]

for (let index = 0; index < inputs.length; index++) {
    const element = inputs[index];
    console.log(isValid(element))
}