/**
 * 16. 3Sum Closest
 * Given an integer array nums of length n and an integer target, find three integers in nums such that the sum is closest to target.

Return the sum of the three integers.

You may assume that each input would have exactly one solution.

 

Example 1:

Input: nums = [-1,2,1,-4], target = 1
Output: 2
Explanation: The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).
Example 2:

Input: nums = [0,0,0], target = 1
Output: 0
 

Constraints:

3 <= nums.length <= 1000
-1000 <= nums[i] <= 1000
-104 <= target <= 104
 */

/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
 var threeSumClosest = function(nums, target) {
    // sorting
    nums = nums.sort((a,b) => a - b)
    let closest = null;

    for (let i = 0; i < nums.length; i++) {
       
        let j = i+1;
        let k = nums.length - 1;

        while (j < k) {
            let sum = nums[i] + nums[j] + nums[k];

            // console.log(`
            
            //     nums: ${nums}
            //     nums[i] = ${nums[i]}
            //     nums[j] = ${nums[j]}
            //     nums[k] = ${nums[k]}
            //     sum = ${sum}

            
            // `)
            // console.log(`Math.abs(target - sum) : ${Math.abs(target - sum)}`)
            // console.log(`Math.abs(target - closest) : ${Math.abs(target - closest)}`)
            if(closest==null) {
                closest = sum;
            } else {
                // sum matches target 
                if(sum === target) {
                    closest = sum;
                    return closest;
                }
                // else 
                
                if((Math.abs(target - sum)) < (Math.abs(target - closest))) {
                    closest = sum;
                }
            }
            if(sum < target) {
                j++
            } else {
                k--
            }
        }
    }
    return closest;
};

console.log(threeSumClosest([-1,2,1,-4], 1))
console.log(threeSumClosest([0,2,1,-3], 1))
console.log(threeSumClosest([1,1,1,0], 100))

console.log(threeSumClosest([1,2,4,8,16,32,64,128], 82))


