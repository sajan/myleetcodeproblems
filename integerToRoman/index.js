/**
 * 12. Integer to Roman
 * Roman numerals are represented by seven different symbols: I, V, X, L, C, D and M.

Symbol       Value
I             1
V             5
X             10
L             50
C             100
D             500
M             1000
For example, 2 is written as II in Roman numeral, just two one's added together. 12 is written as XII, which is simply X + II. The number 27 is written as XXVII, which is XX + V + II.

Roman numerals are usually written largest to smallest from left to right. However, the numeral for four is not IIII. Instead, the number four is written as IV. Because the one is before the five we subtract it making four. The same principle applies to the number nine, which is written as IX. There are six instances where subtraction is used:

I can be placed before V (5) and X (10) to make 4 and 9. 
X can be placed before L (50) and C (100) to make 40 and 90. 
C can be placed before D (500) and M (1000) to make 400 and 900.
Given an integer, convert it to a roman numeral.

 

Example 1:

Input: num = 3
Output: "III"
Explanation: 3 is represented as 3 ones.
Example 2:

Input: num = 58
Output: "LVIII"
Explanation: L = 50, V = 5, III = 3.
Example 3:

Input: num = 1994
Output: "MCMXCIV"
Explanation: M = 1000, CM = 900, XC = 90 and IV = 4.
 

Constraints:

1 <= num <= 3999
 */

/**
 * @param {number} num
 * @return {string}
 */
 var intToRoman = function(num) {
    
    let i = 0;
    let arr = [];
    while (num > 0) {
        let remainder = (num % 10) * (Math.pow(10, i))
        arr.unshift(remainder);
        num = parseInt(num/10);
        i++;
    }

    // console.log({arr})
    let roman = '';
    let map = {
        1: 'I',
        5 : 'V',
        10 : 'X',
        50: 'L',
        100: 'C',
        500: 'D',
        1000: 'M'
    }
    arr = arr.reverse();
    for (let i = 0; i < arr.length; i++) {
        const m = 5 * Math.pow(10, i);
        // console.log(`
        // arr[i]: ${arr[i]}
        // m: ${m}
        // i: ${i}
        
        // `)
        if(arr[i] < m) {
            
            if(arr[i] === (m-Math.pow(10,i))) {
                let d = map[Math.pow(10, i)] + map[m];
                roman = d + roman;
            } else {
                let j = arr[i]/ Math.pow(10, i);
                for (let k = 0; k < j; k++) {
                    roman = map[Math.pow(10, i)] + roman      
                }
            }
            
        } 

        if(arr[i] === m) {
            roman = map[m] + roman; 
        }

        if(arr[i] > m) {
            let d = arr[i] - m;
            let romanD;
            let j = d/ Math.pow(10, i);
            if(j === 4) {
                romanD = map[Math.pow(10,i)] + map[Math.pow(10, i+1)];
            } else {
                romanD =  map[m];
                for (let k = 0; k < j; k++) {
                    romanD = romanD + map[Math.pow(10, i)]
                }
            }
            roman = romanD + roman; 
        }

    }
    return roman;
};


console.log(intToRoman(1994))