/**
 * Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.

A subarray is a contiguous part of an array.

 

Example 1:

Input: nums = [-2,1,-3,4,-1,2,1,-5,4]
Output: 6
Explanation: [4,-1,2,1] has the largest sum = 6.
Example 2:

Input: nums = [1]
Output: 1
Example 3:

Input: nums = [5,4,-1,7,8]
Output: 23
 */

/**
 * @param {number[]} nums
 * @return {number}
 */
//  var maxSubArray = function(nums) {
//     if(nums.length === 0) return 0;
//     if(nums.length === 1) return nums[0];

//     let subs = [];
//     let currentSum = 0;
//     for (let i = 0; i < nums.length; i++) {
//        if(currentSum < 0) {
//            currentSum = 0;
//        } 
//        currentSum += nums[i]; 
//        subs.push(currentSum);
//     }

//     return Math.max(...subs)

// };

var maxSubArray = function(nums) {
    if(nums.length === 0) return 0;
    if(nums.length === 1) return nums[0];

    let max = nums[0];
    let sum = nums[0];
    for (let i = 1; i < nums.length; i++) {
      sum = Math.max(sum+nums[i], nums[i]);
      max = Math.max(max, sum)
    }

    return max;
   

};

console.log(maxSubArray([-2,1,-3,4,-1,2,1,-5,4]))
console.log(maxSubArray([2]))
console.log(maxSubArray([5,4,-1,7,8]))