/**
 * 5. Longest Palindromic Substring
 * Given a string s, return the longest palindromic substring in s.

 

Example 1:

Input: s = "babad"
Output: "bab"
Explanation: "aba" is also a valid answer.
Example 2:

Input: s = "cbbd"
Output: "bb"
 

Constraints:

1 <= s.length <= 1000
s consist of only digits and English letters.
 */


// Copied from Palindrom folder
function isPalindrome(x) {
    const str = x.toString();
    for (let i = 0, j = (str.length - 1); i < str.length, i <= j; i++, j--) {
        if (str[i] !== str[j]) return false
    }
    return true
}

// brute force
/**
 * @param {string} s
 * @return {string}
 */

//  var longestPalindrome = function(s) {
//     let palindroms = [];

//     for (let i = 0; i < s.length; i++) {
//         for (let j = i; j < s.length; j++) {
//             if(s[i] === s[j]) {
//                 let subString = s.substring(i, j+1);
//                 if(isPalindrome(subString)) {
//                     palindroms.push(subString)
//                 }
//             }
//         }
//     }
//     palindroms = palindroms.sort((a,b) => a.length - b.length);
//     return palindroms[palindroms.length - 1]
// };

// Dynamic Solution
/**
 * @param {string} s
 * @return {string}
 */
//  let longestPalindromStr="";
var longestPalindrome = function (s) {
    let longestPalindromStr = [];
    
    isPalindrome(s, longestPalindromStr);
    return longestPalindromStr;
}



function isPalindrome(str, longestPalindromStr) {
    // console.log(str)

    if (str.length === 1) {
        return true;
    }
    let i = 0;
    let j = str.length - 1;
    if (str[i] === str[j]) {
        if (isPalindrome(str.substring(i + 1, j), longestPalindromStr)) {
            longestPalindromStr.push(str)
            // if (longestPalindromStr.length < str.length) {
            //     longestPalindromStr = str;
            // }
            return true;
        }
        return false;
    } else {
        return isPalindrome(str.substring(i + 1, j), longestPalindromStr)
    }
   

}


console.log(longestPalindrome('abbac'))