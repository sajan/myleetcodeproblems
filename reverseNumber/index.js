/**
 * 
 * 7. Reverse Integer
Given a signed 32-bit integer x, return x with its digits reversed. If reversing x causes the value to go outside the signed 32-bit integer range [-231, 231 - 1], then return 0.

Assume the environment does not allow you to store 64-bit integers (signed or unsigned).

 

Example 1:

Input: x = 123
Output: 321
Example 2:

Input: x = -123
Output: -321
Example 3:

Input: x = 120
Output: 21
 

Constraints:

-231 <= x <= 231 - 1
 */

var reverse = function(x) {
    let number = x;
    let isNegativeNumber = false;
    if(number < 0) {
        isNegativeNumber = true;
        number = Math.abs(number)
    }
    let newNumber = 0;
    let i = 0;
    while(number > 0) {
        let remainder = number % 10;
        number = parseInt(number/10);
        if(i == 0) {
            newNumber = remainder;
            i++
        } else {
            newNumber = newNumber * 10;
            newNumber = newNumber + remainder;
        }
    }
    return newNumber > 0x7FFFFFFF ? 0 : (isNegativeNumber ? (newNumber * -1) : newNumber);

};

console.log(reverse(1534236469))