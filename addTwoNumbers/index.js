/**
 * Definition for singly-linked list.

 */
var ListNode = /** @class */ (function () {
    function ListNode(val, next) {
        this.val = (val === undefined ? 0 : val);
        this.next = (next === undefined ? null : next);
    }
    return ListNode;
}());
var addTwoNumbers = function (l1, l2) {
    var sum = new ListNode(0), curDigit = 0, carry = 0, head = sum;
    while (l1 || l2 || carry > 0) {
        var val1 = l1 ? l1.val : 0, val2 = l2 ? l2.val : 0; // storing values we are working with for this iteration. 
        curDigit = (val1 + val2 + carry) % 10;
        carry = Math.floor((val1 + val2 + carry) / 10);
        l1 = l1 ? l1.next : l1; // increment l1 and l2 to the next node, making sure not accessing property of null
        l2 = l2 ? l2.next : l2;
        var node = new ListNode(curDigit, null); // create single node in list with curDigit and append to head of linked list
        head.next = node;
        head = head.next;
    }
    return sum.next;
};
console.log(addTwoNumbers([2, 4, 3, 4], [5, 6, 4, 7]));
