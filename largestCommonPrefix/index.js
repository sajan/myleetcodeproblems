/**
 * Write a function to find the longest common prefix string amongst an array of strings.

If there is no common prefix, return an empty string "".

 

Example 1:

Input: strs = ["flower","flow","flight"]
Output: "fl"
Example 2:

Input: strs = ["dog","racecar","car"]
Output: ""
Explanation: There is no common prefix among the input strings.
 

Constraints:

1 <= strs.length <= 200
0 <= strs[i].length <= 200
strs[i] consists of only lower-case English letters.
 */

var longestCommonPrefix = function(strs) {
    let compareStr;
    if(strs.length === 1) return strs[0]
    if(strs[0] === "") return ""
    
    let resultString = "";

    
    for (let i = 0; i < strs.length; i++) {
        if(i === 0) {
            resultString = strs[0];
            // console.log(`Compare String : ${compareStr}`)

            continue;
        }
        else {
            compareStr = resultString;
        }
        // console.log(`Compare String : ${compareStr}`)
        let str = strs[i];
        if(str === "") return ""

        resultString = "";
        for (let j = 0; j < str.length; j++) {
            const ele = str[j];
            // console.log(`Ele : ${ele} Str : ${str[j]}`)
            
            if(ele === compareStr[j]) {
                resultString+=(ele);
            } else {
                break;
            }
        }
        
        
    }
    return resultString
    
};

console.log(longestCommonPrefix(["flower","flow","flight"]))
console.log(longestCommonPrefix(["dog","racecar","car"]))
console.log(longestCommonPrefix(["ab","a"]))
console.log(longestCommonPrefix(["ab"]))
console.log(longestCommonPrefix([""]))
console.log(longestCommonPrefix(["cir","car"]))
console.log(longestCommonPrefix(["aaa","aa","aaa"]))
